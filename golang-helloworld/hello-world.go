package main

import "fmt"

// Very basic hello world.  Most just a template.
func main() {
    fmt.Println("hello world")
}
